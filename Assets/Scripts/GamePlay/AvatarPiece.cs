﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using UnityEngine.Networking;

public class AvatarPiece : NetworkBehaviour
{
    public enum PieceType { head, leftHand, rightHand, label };
    public PieceType currentType;
    public ulong ownerID;

    public Transform target;

    public override void OnStartAuthority()
    {
        Init();
    }

    // Use this for initialization
    void Init()
    {
        print("Piece auth?");
        switch (currentType)
        {
            case PieceType.head:
                target = GameManager.instance.vrPlayer.Head.transform;
                // Update player name
                transform.GetChild(0).GetComponent<TextMesh>().text = SteamFriends.GetFriendPersonaName(new CSteamID(GameManager.instance.networkPlayer.GetComponent<PlayerController>().steamID));
                if (target != null)
                {
                    StartCoroutine(TrackTarget());
                }
                break;
            case PieceType.leftHand:
                target = GameManager.instance.vrPlayer.LeftHand.transform;
                if (target != null)
                {
                    StartCoroutine(TrackTarget());
                }
                break;
            case PieceType.rightHand:
                target = GameManager.instance.vrPlayer.RightHand.transform;
                if (target != null)
                {
                    StartCoroutine(TrackTarget());
                }
                break;
            default:
                break;
        }
    }

    IEnumerator TrackTarget ()
    {
        while(hasAuthority)
        {
            transform.position = target.position;
            transform.rotation = target.rotation;
            yield return null;
        }
    }

    [ClientRpc]
    public void RpcGetType(int type)
    {
        print("Av RPC");
        switch (type)
        {
            case 0:
                currentType = PieceType.head;
                break;
            case 1:
                currentType = PieceType.leftHand;
                break;
            case 2:
                currentType = PieceType.rightHand;
                break;
            default:
                break;
        }

        Init();
    }
}
