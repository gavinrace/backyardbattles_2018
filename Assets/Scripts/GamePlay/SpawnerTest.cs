﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using UnityEngine.Networking;
using System;

public class SpawnerTest : NetworkBehaviour {

    private NetworkConnection conn;

    public GameObject bullet;

    public int[] conncts;

	// Use this for initialization
	public override void OnStartAuthority () {

        if (NetworkServer.active && NetworkServer.connections.Count > 0)
        {
            CmdSpawnPiece(GetComponent<NetworkPlayer>().steamId);
        }
	}
	
	// Update is called once per frame
	void Update () {

        if (hasAuthority)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                print(SteamNetworkManager.Instance.myClient);
                CmdSpawnPiece(GetComponent<NetworkPlayer>().steamId);
            }

        }
	}

    [Command]
    void CmdSpawnPiece(ulong id)
    {
        Debug.Log("ID passed in " + id);
        //var conn = NetworkServer.connections[0];
        

        print("Steam connection = " + SteamNetworkManager.Instance.ClientConnectionWrapper(id));

        NetworkConnection conn = SteamNetworkManager.Instance.ClientConnectionWrapper(id);
        print("Is it ready?" + conn.isReady);

        if (conn.isReady)
        {
            GameObject theBullet = Instantiate(bullet);
            //theBullet.GetComponent<Bullet>().thePlayer = transform.GetChild(0);
            NetworkServer.SpawnWithClientAuthority(theBullet, conn);
        }
        else
        {
            conn.RegisterHandler(MsgType.Ready, OnReady);
        }

        
    }

    private void OnReady(NetworkMessage netMsg)
    {
        print("Should be ready!" + conn.isReady);
        GameObject theBullet = Instantiate(bullet);
        NetworkServer.SpawnWithClientAuthority(theBullet, conn);
    }

}
