﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AmmoHandler : NetworkBehaviour
{

    public ulong steamID;

    public GameObject ammo;
    public float ammoDelay;

    public int currentAmmo;
    public int maxAmmo = 5;


    public void StartAmmoSpawn(ulong id)
    {
        //first get steamID && conn
        steamID = id;

        print("does " + steamID + " one have auth? " + hasAuthority);
        if (hasAuthority)
        {
            StartCoroutine(AmmoSpawnDelay());
        }

    }

    [Command]
    void CmdSpawnAmmo(ulong id)
    {
        NetworkConnection conn = SteamNetworkManager.Instance.ClientConnectionWrapper(id);

        if (conn.isReady)
        {
            //spawn pieces
            GameObject newAmmo = Instantiate(ammo);
            NetworkServer.SpawnWithClientAuthority(newAmmo, conn);
        }
    }

    IEnumerator AmmoSpawnDelay()
    {
        if (hasAuthority && currentAmmo < maxAmmo)
        {
            yield return new WaitForSeconds(ammoDelay);
            currentAmmo++;
            CmdSpawnAmmo(steamID);
            StartCoroutine(AmmoSpawnDelay());
        }
        else
        {
            yield return null;
        }
    }
}