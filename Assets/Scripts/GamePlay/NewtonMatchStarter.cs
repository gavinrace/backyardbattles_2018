﻿using UnityEngine;
using System.Collections;
using NewtonVR;

namespace NewtonVR.Example
{
    public class NewtonMatchStarter : MonoBehaviour
    {
        public NVRButton Button;

        public enum state
        {
            findMatch,
            inviteFriend
        }

        public state buttonState;
        private void Update()
        {
            if (Button.ButtonDown)
            {
                if (buttonState == state.findMatch)
                {
                    SteamNetworkManager.Instance.FindMatch();
                }
                else
                {
                    SteamNetworkManager.Instance.InviteFriendsToLobby();
                }
                
            }

            if (Input.GetKeyDown(KeyCode.F))
            {
                SteamNetworkManager.Instance.FindMatch();
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                SteamNetworkManager.Instance.InviteFriendsToLobby();
            }
        }
    }
}
