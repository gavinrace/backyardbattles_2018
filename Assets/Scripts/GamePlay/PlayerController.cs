﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Steamworks;
using UnityEngine.Networking.NetworkSystem;

public class PlayerController : NetworkBehaviour
{
    [SyncVar(hook = "SetID")]
    public ulong steamID;

    [SyncVar]
    public bool isHomePlayer;

    public enum state
    {
        init,
        spawned
    }

    public state playerState = state.init;


    public override void OnStartServer()
    {
        base.OnStartServer();

        StartCoroutine(SetNameWhenReady());
    }

    private void SetID(ulong id)
    {
        steamID = id;
        print("setting ID to " + id);

        //once we properly have our id, we can start spawning
        //spawn avatar on local player
        if (hasAuthority && playerState == state.init)
        {
            GameManager.instance.networkPlayer = gameObject;

            playerState = state.spawned;
            SpawnPlayer();
        }
    }

    public void SpawnPlayer()
    {
        GetComponent<AvatarHandler>().SpawnAvatar(steamID);
        GetComponent<AmmoHandler>().StartAmmoSpawn(steamID);
    }

    IEnumerator SetNameWhenReady()
    {
        // Wait for client to get authority, then retrieve the player's Steam ID
        var id = GetComponent<NetworkIdentity>();
        while (id.clientAuthorityOwner == null)
        {
            yield return null;
        }

        steamID = SteamNetworkManager.Instance.GetSteamIDForConnection(id.clientAuthorityOwner).m_SteamID;
        print("running");

        //set player home or away
        NetworkConnection conn = SteamNetworkManager.Instance.ClientConnectionWrapper(steamID);
        if (conn.connectionId == 0)
        {
            isHomePlayer = true;
        }

        RpcSetSteamID(steamID);

    }

    [ClientRpc]
    void RpcSetSteamID(ulong id)
    {
        Debug.Log("This ID RPC:" + id);
        GetComponent<AvatarHandler>().steamID = id;
        GetComponent<AmmoHandler>().steamID = id;
    }
}