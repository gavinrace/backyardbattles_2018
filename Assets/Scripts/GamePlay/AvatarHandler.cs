﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using UnityEngine.Networking;
using System;

public class AvatarHandler : NetworkBehaviour
{
    public ulong steamID;

    public GameObject headPrefab;
    public GameObject handRPrefab;
    public GameObject handLPrefab;

    public GameObject currentHead;
    public GameObject currentLHand;
    public GameObject currentRHand;


    public void SpawnAvatar(ulong id)
    {
        print("Time to spawn!");
        //first get steamID && conn
        steamID = id;
        print(id);

        print("does " + steamID + " one have auth? " + hasAuthority);
        if (hasAuthority)
        {
            CmdSpawnPiece(steamID);
        }
        
    }

    [Command]
    void CmdSpawnPiece(ulong id)
    {
        NetworkConnection conn = SteamNetworkManager.Instance.ClientConnectionWrapper(id);

        if (conn.isReady)
        {
            print("spawn pieces");
            GameObject currentHead = Instantiate(headPrefab);
            //currentHead.GetComponent<AvatarPiece>().currentType = AvatarPiece.PieceType.head;
            NetworkServer.SpawnWithClientAuthority(currentHead, conn);
            currentHead.GetComponent<AvatarPiece>().RpcGetType(0);

            GameObject currentLHand = Instantiate(handLPrefab);
            //currentLHand.GetComponent<AvatarPiece>().currentType = AvatarPiece.PieceType.leftHand;
            NetworkServer.SpawnWithClientAuthority(currentLHand, conn);

            GameObject currentRHand = Instantiate(handRPrefab);
            //currentRHand.GetComponent<AvatarPiece>().currentType = AvatarPiece.PieceType.rightHand;
            NetworkServer.SpawnWithClientAuthority(currentRHand, conn);

        }/*
        else
        {
            conn.RegisterHandler(MsgType.Ready, OnReady);
        }*/
    }

    /*private void OnReady(NetworkMessage netMsg)
    {
        if (hasAuthority && steamID != null)
        {
            CmdSpawnPiece(steamID);
        }
    }*/

}