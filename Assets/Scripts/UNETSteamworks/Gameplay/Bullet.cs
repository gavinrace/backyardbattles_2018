﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bullet : NetworkBehaviour
{

    public float speed;

    public Transform thePlayer;
    GameObject[] players;


    public override void OnStartAuthority()
    {
        //print("this version: " + GetComponent<NetworkIdentity>().hasAuthority);
        players = GameObject.FindGameObjectsWithTag("Player");
        print(players.Length);

        foreach (GameObject player in players)
        {
            if (!player.GetComponent<Rigidbody>().isKinematic)
            {
                thePlayer = player.gameObject.transform.GetChild(0);
                StartCoroutine(UpdatePos());
                print("auth? " + hasAuthority);
            }
        }
    }

    IEnumerator UpdatePos()
    {
        //transform.Translate(transform.forward * Time.deltaTime * speed);
        while (true && hasAuthority)
        {

                Vector3 pos = new Vector3(thePlayer.position.x, thePlayer.position.y, thePlayer.position.z);
                transform.position = pos;
                yield return null;

        }
    }
}